package com.whereta.dao;

import com.whereta.model.MessageBoardWithBLOBs;

/**
 * Created by vincent on 15-9-24.
 */
public interface IMessageBoardDao {

    int save(MessageBoardWithBLOBs messageBoard);
}
