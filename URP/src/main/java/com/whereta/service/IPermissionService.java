package com.whereta.service;

import com.whereta.vo.PermissionCreateVO;
import com.whereta.vo.PermissionEditVO;
import com.whereta.vo.ResultVO;

/**
 * @author Vincent
 * @time 2015/8/28 9:44
 */
public interface IPermissionService {

    ResultVO getShowPermissions(Integer rootId);

    ResultVO getMenuShowPermissions(int menuId);

    ResultVO getRoleShowPermissions(int roleId);

    ResultVO cratePermission(PermissionCreateVO permissionCreateVO);

    ResultVO editPermission(PermissionEditVO permissionEditVO);

    ResultVO delPermission(int perId);
}
